import { View, Text,Image,StyleSheet } from 'react-native'
import React,{useState} from 'react'
import Button from '../../components/Button'
import images from '../../utility/ImagePath'
import ImagePicker from 'react-native-image-crop-picker';


export default function ScreenStatus() {
  const [images,setImage] = useState('https://reactnative.dev/img/tiny_logo.png')
  const take_photo_from_camera = () => {
    ImagePicker.openCamera({
      width: 200,
      height: 200,
      cropping: true,
    }).then(image => {
      setImage(image.path)
      console.log(image);
    });
  }

  const take_photo_from_gallery  = () => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: true
    }).then(image => {
      setImage(image.path)
      console.log(image);
    });
  }

  return (
    <View>
      <Text style = {{color : "green"}}>ScreenStatus</Text>
      <Image style={styles.logoView} source={{
          uri: images,
        }} />
      <Button title = "Take Photo From Camera" onPressButton={take_photo_from_camera}/>
      <Button title = "Take Photo From Gallery" onPressButton={take_photo_from_gallery}/>
    </View>
  )
}

const styles = StyleSheet.create({
  logoView: {
    width: 100,
    height: 100,
    alignSelf: 'center',
    marginVertical: 20,
  },
})