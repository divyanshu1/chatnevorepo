// import { View, Text,FlatList } from 'react-native'
// import React,{useEffect} from 'react'
// import { useDispatch,useSelector } from 'react-redux';
// import { fetch_data } from '../../redux-modules/UserModule/action';


// export default function ScreenChat() {
//   const users = useSelector((state) => state.user)
//   const dispatch = useDispatch()
//   useEffect(() => {
//     dispatch(fetch_data())
//   },[])
//   const {name} = users.user_data
//   console.log("USERS",users.user_data)
//   return (
//     <View>
//       <Text style = {{color : "green",marginVertical : 20}}>ScreenChat</Text>
       
//           {users.loading ? <Text style = {{color : "green"}}>Loading</Text>:
//           users.user_data.map((value) => (
//             <Text style = {{color : "green"}}>{value.name}</Text>
//           ))
//           }
//         </View>
//   )
// }


import {View, Text, FlatList} from 'react-native';
import React,{useEffect} from 'react'
import ChatListProfile from '../../components/ChatListProfile';
import { useDispatch,useSelector } from 'react-redux';
import { fetch_data } from '../../redux-modules/UserModule/action';

export default function ScreenChats({navigation}) {
  const users = useSelector((state) => state.user)
   const dispatch = useDispatch()
   useEffect(() => {
     dispatch(fetch_data())
   },[])
   console.log("USER_Data",users.user_data)
   const Data = users.user_data
   console.log("data",Data)

  return (
    <View>
      <FlatList
        data = {Data}
        renderItem={item => (
          // console.log("ITEM",item.item.name)
          <ChatListProfile
          Name={item.item.name}
          website = {item.item.website}
          onPress={() => navigation.navigate('ScreenProfile',{name : item.item.name})}
          />
        )}
        keyExtractor={item => item.id}
      />
      
    </View>
  );
}


