import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import ImageBg from '../../assets/images/androidBG.jpg';
import ProfileHeader from '../../components/ProfileHeader';
import {styles} from './StyleProfile';
import InputBox from '../../components/InputBox';
import MessageList from '../../components/MessageList';

export default function ScreenProfile(props) {
  // const [textInput, setTextInput ] = useState('');
  console.log("PROPS Screen Profile",props.route.params.name)

  
  return (
    <View style={styles.container}>
     <ProfileHeader name = {props.route.params.name}/>
     <ImageBackground source={ImageBg} resizeMode="cover" style={{flex: 1}}>
     <ScrollView>
          <MessageList />
        </ScrollView>

        <InputBox
          // onChangeText={text => setTextInput(text)}
          // value={textInput}
          // onPress={addTodo}
        />
      </ImageBackground>
    </View>
  );
}


// import {
//   View,
//   Text,
//   ImageBackground,
//   ScrollView,
//   FlatList,
//   TouchableOpacity,
//   Alert,
// } from 'react-native';
// import React, {useState, useEffect} from 'react';
// import ImageBg from '../../assets/images/androidBG.jpg';
// import ProfileHeader from '../../components/ProfileHeader';
// import {styles} from './StyleProfile';
// import InputBox from '../../components/InputBox';
// import MessageList from '../../components/MessageList';
// export default function ScreenProfile({navigation}) {
//   // const [textInput, setTextInput ] = useState('');
//   return (
//     <View style={styles.container}>
//       <ProfileHeader onPress={() => navigation.navigate('CHATS')} />
//       <ImageBackground source={ImageBg} resizeMode="cover" style={{f
//         <ScrollView>
//           <MessageList />
//         </ScrollView>
//         <InputBox
//           // onChangeText={text => setTextInput(text)}
//           // value={textInput}
//           // onPress={addTodo}
//         />
//       </ImageBackground>
//     </View>
//   );
// }

