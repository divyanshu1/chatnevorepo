import {StyleSheet} from 'react-native';
import { colors } from '../../utility/Color';
import { fonts } from '../../utility/fonts';

const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent : "center",
        alignItems : "center"
    },
    text : {
        color : "green"
    },
    logoView: {
        width: 250,
        height: 250,
        alignSelf: 'center',
        // marginVertical: 20,
      },
      welcomeText: {
        color: colors.BlueColor,
        fontSize: 28,
        textAlign: 'center',
        marginVertical: 30,
        fontWeight : fonts.extraBold
      },
      policyText: {
        color: colors.BlackColor,
        marginHorizontal: 15,
        fontSize: 13,
        textAlign: 'center',
      },
      blueText: {
        color: colors.BlueColor,
      },
      agreeBtn: {
        backgroundColor: colors.BlueColor,
        marginHorizontal: 40,
        paddingVertical: 10,
        borderRadius: 6,
        marginTop: 25,
        marginBottom: 50,
      },
      btnText: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.BlackColor,
        width : 200,
        fontWeight: fonts.semiBold,
      },
      binmileText: {
        textAlign: 'center',
        letterSpacing: 2,
        marginVertical: 5,
        color: colors.BlackColor,
        fontWeight: fonts.semiBold,
      },
})
export default styles