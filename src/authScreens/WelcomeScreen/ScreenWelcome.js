import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import styles from './StyleWelcome';
import images from '../../utility/ImagePath';
import {constants} from '../../utility/Constants';
import Button from "../../components/Button";

export default function ScreenWelcome({navigation}) {
  return (
    <View style={styles.container}>
      <Image style={styles.logoView} source={images.welcomeImage} />
      <Text style={styles.welcomeText}>{constants.welcomeTitle}</Text>
      <Text style={styles.policyText}>
        {constants.readOur}
        <Text style={styles.blueText}>{constants.privacyPolicy}</Text>{' '}
        {constants.agreeAndContinue}
        <Text style={styles.blueText}>{constants.termAndService}</Text>
      </Text>
      {/* <TouchableOpacity
        style={styles.agreeBtn}
        onPress={() => navigation.navigate('ScreenLogin')}>
        <Text style={styles.btnText}>{constants.agreeBtn} </Text>
      </TouchableOpacity> */}
      <Button title = {constants.agreeBtn} 
      onPressButton={() => navigation.navigate("ScreenLogin")}/>
      <Text style={styles.policyText}>{constants.from}</Text>
      <Text style={styles.binmileText}>{constants.company}</Text>
    </View>
  );
}
