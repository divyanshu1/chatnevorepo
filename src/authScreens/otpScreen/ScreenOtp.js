import { NavigationContainer } from '@react-navigation/native';
import React, {useState} from 'react';
import {TextInput,View,Text} from 'react-native';
import Button from '../../components/Button';
import { constants } from '../../utility/Constants';
import { styles } from './StyleOtp';

export default function ScreenOtp(props) {
  const [otp,confirmOTP] = useState("");
return (
  <View style = {{flex : 1,justifyContent : "center",alignItems : "center"}}>
    <Text style = {{color : "#0BA8E6"}}>{constants.enterOtp}</Text>
      <TextInput
        placeholder="      Enter OTP"
        placeholderTextColor="#0BA8E6"
        keyboardType="numeric"
        color = "#0BA8E6"
        style={styles.loginInputBox}
        maxLength={6}
        keyboardType = 'number-pad'
        onChangeText={(val) => confirmOTP(val)}
      />


<Button title = {constants.confirmOtp} onPressButton={() => props.navigation.navigate("AppNavigations")}/>
  </View>
)
}

