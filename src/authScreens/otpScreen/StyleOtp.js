import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
    loginInputBox: {
      borderWidth: 1,
      borderRadius : 20,
      width : 200,
      borderColor: '#0BA8E6',
      margin: 30,
    },
    loginNextBtn: {
      backgroundColor: '#0BA8E6',
      alignSelf: 'center',
      paddingHorizontal: 40,
      paddingVertical: 10,
      borderRadius: 5,
      marginTop : 40,
    },
  })

