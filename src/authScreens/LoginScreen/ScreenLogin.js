import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {constants} from '../../utility/Constants';
import styles from './StyleLogin';
import Button from '../../components/Button';
export default function ScreenLogin({navigation}) {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
          <Text style={styles.loginTitle}>{constants.enterMobile}</Text>
          <Text style={styles.loginVerifyText}>
            {constants.needToVerifyText}
          </Text>
          <Text style={styles.loginVerifyNum}>{constants.whatMyNum}</Text>
          <TextInput
            placeholder="(+91)  phone number"
            placeholderTextColor="black"
            style={styles.loginInputBox}
          />
          {/* <TouchableOpacity
            style={styles.loginNextBtn}
            onPress={() => navigation.navigate('ScreenOtp')}>
            <Text style={styles.btnText}>{constants.nextBtn}</Text>
          </TouchableOpacity> */}
          <Button title = {constants.nextBtn} 
          onPressButton={() => {navigation.navigate('ScreenOtp')}}/>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
