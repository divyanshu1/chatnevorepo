import {StyleSheet} from 'react-native';
import { colors } from '../../utility/Color';
import { fonts } from '../../utility/Constants';

const styles = StyleSheet.create({
    loginTitle: {
      color: colors.BlueColor,
      fontSize: 22,
      textAlign: 'center',
      fontWeight: '600',
      marginTop: 30,
    },
    loginVerifyText: {
      marginTop: 40,
      textAlign: 'center',
      color: colors.BlackColor,
      fontSize: 16,
    },
  
    loginVerifyNum: {
      color: colors.BlueColor,
      textAlign: 'center',
      fontSize: 16,
      marginTop: 10,
    },
    loginInputBox: {
      borderTopWidth: 1,
      borderTopColor: colors.BlueColor,
      borderBottomColor: colors.BlueColor,
      borderBottomWidth: 1,
      marginHorizontal: 20,
      paddingHorizontal: 20,
      marginTop: 50,
      color : colors.BlueColor
    },
    loginNextBtn: {
      backgroundColor: colors.BlueColor,
      alignSelf: 'center',
      paddingHorizontal: 40,
      paddingVertical: 10,
      borderRadius: 5,
      marginTop: 240,
    },
    btnText: {
      fontSize: 16,
      textAlign: 'center',
      color: colors.BlackColor,
      fontWeight: fonts.semiBold,
    },
  });
  
  export default styles;
  