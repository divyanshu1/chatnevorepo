
import { createStore} from "redux";
import { applyMiddleware } from "redux";
import thunk from "redux-thunk";
import root_reducer_1 from "../redux-modules/root_reducer/Root_reducer";

const store = createStore(root_reducer_1,applyMiddleware(thunk))

export default store