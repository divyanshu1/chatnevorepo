export const fonts = {
  light: 'Nunito-Light',
  regular: 'Nunito-Regular',
  bold: 'Nunito-Bold',
  extraBold: 'Nunito-ExtraBold',
  Bold: 'bold',
  semiBold: '600',
};
