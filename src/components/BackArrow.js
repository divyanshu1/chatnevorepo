import { StyleSheet, TouchableOpacity ,View } from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import { colors } from '../utility/Color';


export default function BackArrow({backPress = () => {}}) {
  return (
    <View>
        <TouchableOpacity onPress={backPress}>
            <Feather
                name="arrow-left"
                size={25}
                color={colors.BlackColor}
                style={{marginVertical: 10}}
            />
        </TouchableOpacity>
      
    </View>
  )
}
