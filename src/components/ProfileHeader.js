import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import LeftArrow from 'react-native-vector-icons/AntDesign';
import {colors} from '../utility/Color';
import myImg from '../assets/images/prakashPhoto.jpg';
import {constants} from '../utility/Constants';
import DotIcon from 'react-native-vector-icons/Entypo';
import {fonts} from '../utility/fonts';

export default function ProfileHeader(props) {
  console.log("Profile Header",props)
  return (
    <View style={styles.headerContainer}>
      <View style={styles.headerRow}>
        <TouchableOpacity>
          <LeftArrow name="arrowleft" style={styles.headerIcons} />
        </TouchableOpacity>
        <Image source={myImg} style={styles.profileImage} />
        {/* <Text style = {styles.profileImage}>{profile_name[0]}</Text> */}
        <Text style={styles.headerName}>{props.name}</Text>
      </View>
      <TouchableOpacity>
        <DotIcon name="dots-three-vertical" style={styles.headerIcons} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: colors.BlueColor,
    flexDirection: 'row',
    paddingVertical: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerIcons: {
    color: colors.BlackColor,
    fontSize: 25,
    marginHorizontal: 8,
  },
  profileImage: {
    height: 40,
    width: 40,
    borderRadius: 50,
    backgroundColor : colors.LightGreyColor,
    alignItems :"center"
  },
  headerName: {
    color: colors.BlackColor,
    fontSize: 20,
    fontWeight: fonts.extraBold,
    marginHorizontal: 10,
  },
  headerRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
