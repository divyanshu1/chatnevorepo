import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {colors} from '../utility/Color';
import {fonts} from '../utility/fonts';

export default function MessageList() {
  return (
    <View>
      <Text style={styles.chatTextRight}>Hello</Text>
      <Text style={styles.chatTextLeft}>Hii &#128530; </Text>
      <Text style={styles.chatTextRight}>I'm Prakash</Text>
      <Text style={styles.chatTextLeft}>I'm Divyanshu</Text>
      <Text style={styles.chatTextRight}>Where are you from</Text>
      <Text style={styles.chatTextLeft}>I'm from noida, What about you</Text>
      <Text style={styles.chatTextRight}>i'm from delhi &#128540;</Text>
      <Text style={styles.chatTextLeft}>okay</Text>

      <Text style={styles.chatTextRight}>Hello</Text>
      <Text style={styles.chatTextLeft}>Hii &#128530; </Text>
      <Text style={styles.chatTextRight}>I'm Prakash</Text>
      <Text style={styles.chatTextLeft}>I'm Divyanshu</Text>
      <Text style={styles.chatTextRight}>Where are you from</Text>
      <Text style={styles.chatTextLeft}>I'm from noida, What about you</Text>
      <Text style={styles.chatTextRight}>i'm from delhi &#128540;</Text>
      <Text style={styles.chatTextLeft}>okay</Text>

      <Text style={styles.chatTextRight}>Hello</Text>
      <Text style={styles.chatTextLeft}>Hii &#128530; </Text>
      <Text style={styles.chatTextRight}>I'm Prakash</Text>
      <Text style={styles.chatTextLeft}>I'm Divyanshu</Text>
      <Text style={styles.chatTextRight}>Where are you from</Text>
      <Text style={styles.chatTextLeft}>I'm from noida, What about you</Text>
      <Text style={styles.chatTextRight}>i'm from delhi &#128540;</Text>
      <Text style={styles.chatTextLeft}>Okay</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  chatTextRight: {
    backgroundColor: colors.BlueColor,
    marginTop: 10,
    paddingVertical: 4,
    fontSize: 16,
    color: colors.BlackColor,
    fontWeight: fonts.semiBold,
    borderRadius: 10,
    textAlign: 'center',
    alignSelf: 'flex-end',
    marginHorizontal: 10,
    paddingHorizontal: 20,
  },
  chatTextLeft: {
    backgroundColor: colors.LightGreyColor,
    marginTop: 10,
    paddingVertical: 4,
    fontSize: 16,
    color: colors.BlackColor,
    fontWeight: fonts.semiBold,
    borderRadius: 10,
    textAlign: 'center',
    alignSelf: 'flex-start',
    marginHorizontal: 10,
    paddingHorizontal: 20,
  },
});
