
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import { colors } from '../utility/Color';
import { fonts } from '../utility/fonts';
import { constants } from '../utility/Constants';
import DotIcon from 'react-native-vector-icons/Entypo';
import SearchIcon from 'react-native-vector-icons/Ionicons';

export default function Header() {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.headerTitle}>{constants.headerTitle}</Text>
      <View style={styles.iconContainer}>
        <TouchableOpacity>
          <SearchIcon name="search" style={styles.headerIcons} />
        </TouchableOpacity>
        <TouchableOpacity>
          <DotIcon name="dots-three-vertical" style={styles.headerIcons} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: colors.BlueColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 30,
    paddingBottom: 15,
  },
  headerTitle: {
    color: colors.WhiteColor,
    fontWeight: fonts.semiBold,
    fontSize: 22,
  },
  iconContainer: {
    flexDirection: 'row',
  },
  headerIcons: {
    marginLeft: 15,
    color: colors.WhiteColor,
    fontSize: 25,
  },
});
