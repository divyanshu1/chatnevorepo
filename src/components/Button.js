
import React from "react";
import {
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { colors } from "../utility/Color";

const Button = ({title = "",onPressButton = () => {},onDisabled = false}) => {
  return (
    <TouchableOpacity
    style = {styles.buttonView}
    onPress = {onPressButton}
    disabled = {onDisabled}
    >
    <Text style = {styles.titleText}>{title}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 18,
    color: colors.WhiteColor,
  },
  buttonView : {
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: 14,
    backgroundColor : colors.BlueColor,
    marginVertical : 20,
    marginHorizontal : 35,
    width : 300
  },
})

export default Button
