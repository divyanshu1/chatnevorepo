// import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
// import React from 'react';
// import myImg from '../assets/images/prakashPhoto.jpg';
// import {colors} from '../utility/Color';
// import { fonts } from '../utility/fonts';

// export default function ChatListProfile(props) {
//   console.log("PROPS",props)
//   return (
//     <TouchableOpacity style={styles.profileContainer} onPress={props.onPress}>
//       <Image source={myImg} style={styles.profileImage} />
//       <View style={styles.profileTextContent}>
//         <View>
//           <Text style={styles.profileTitle}>{props.name}</Text>
//         </View>
//         <View>
//           <Text style={styles.profileTime}>{props.city}</Text>
//         </View>
//       </View>
//     </TouchableOpacity>
//   );
// }

// const styles = StyleSheet.create({
//   profileContainer: {
//     height: 90,
//     alignItems: 'center',
//     backgroundColor: colors.WhiteColor,
//     borderRadius: 10,
//     shadowColor: colors.BlueColor,
//     shadowOpacity: 1,
//     shadowRadius: 8,
//     elevation: 8,
//     flexDirection: 'row',
//     paddingLeft: 16,
//     paddingRight: 14,
//     marginTop: 6,
//     marginHorizontal: 10,
//   },
//   profileImage: {
//     height: 50,
//     width: 50,
//     borderRadius: 50,
//   },
//   profileTextContent: {
//     flex: 1,
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     marginLeft: 20,
//     marginRight: 10,
//   },
//   profileTitle: {
//     color: colors.BlackColor,
//     fontWeight: fonts.semiBold,
//     fontSize: 18,
//   },
//   profileTime: {
//     fontSize: 14,
//     color: colors.GreyColor,
//     fontWeight: fonts.semiBold,
//   },
// });


import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import myImg from '../assets/images/prakashPhoto.jpg';
import {colors} from '../utility/Color';
import { fonts } from '../utility/fonts';

export default function ChatListProfile({Name,website,onPress}) {
  console.log("Name",Name)
  return (
    <TouchableOpacity style={styles.profileContainer} onPress={onPress}>
      <Image source={myImg} style={styles.profileImage} />
      <View style={styles.profileTextContent}>
        <View>
          <Text style={styles.profileTitle}>{Name}</Text>
        </View>
        <View>
          <Text style={styles.profileTime}>{website}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    height: 90,
    alignItems: 'center',
    backgroundColor: colors.WhiteColor,
    borderRadius: 10,
    shadowColor: colors.BlueColor,
    shadowOpacity: 1,
    shadowRadius: 8,
    elevation: 8,
    flexDirection: 'row',
    paddingLeft: 16,
    paddingRight: 14,
    marginTop: 6,
    marginHorizontal: 10,
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  profileTextContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 10,
  },
  profileTitle: {
    color: colors.BlackColor,
    fontWeight: fonts.semiBold,
    fontSize: 18,
  },
  profileTime: {
    fontSize: 14,
    color: colors.GreyColor,
    fontWeight: fonts.semiBold,
  },
});
