import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React from 'react';
import {colors} from '../utility/Color';
import Submit from 'react-native-vector-icons/MaterialCommunityIcons';

export default function InputBox({onChangeText, onPress, value}) {
  return (
    <View style={styles.inputContainer}>
      <TextInput
        placeholder="Message"
        placeholderTextColor={colors.BlackColor}
        style={styles.inputBox}
        onChangeText={onChangeText}
        value={value}
      />
      <TouchableOpacity style={styles.submitBtn} onPress={onPress}>
        <Submit name="arrow-right" style={styles.headerIcons} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
  },
  inputBox: {
    backgroundColor: colors.WhiteColor,
    width: 280,
    borderRadius: 10,
    paddingHorizontal: 30,
    height: 40,
    color: colors.BlackColor,
    fontSize: 16,
  },
  submitBtn: {
    backgroundColor: colors.DarkBlueColor,
    paddingHorizontal: 20,
    // height: 40,
    textAlign: 'center',
    alignItems: 'center',
    paddingVertical: 7,
    borderRadius: 10,
  },
  headerIcons: {
    color: colors.WhiteColor,
    fontSize: 25,
    marginHorizontal: 8,
  },
});
