import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import React from 'react';
import {colors} from '../utility/Color';
import {fonts} from '../utility/fonts';
import SplashImg from '../assets/images/chatlogo1.png';

export default function ScreenSplash() {
  return (
    <View style={styles.splashContainer}>
      <ImageBackground
        source={SplashImg}
        resizeMode="cover"
        style={styles.splashBgImg}></ImageBackground>
    </View>
  );
}
const styles = StyleSheet.create({
  splashContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.BlackColor,
  },
  splashBgImg: {
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
  splashText: {
    color: colors.BlackColor,
    fontSize: 32,
    fontWeight: fonts.Bold,
    textAlign: 'center',
  },
});
