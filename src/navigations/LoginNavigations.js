import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ScreenLogin from '../authScreens/LoginScreen/ScreenLogin';
import ScreenWelcome from '../authScreens/WelcomeScreen/ScreenWelcome';
import ScreenOtp from '../authScreens/otpScreen/ScreenOtp';
import AppNavigations from './AppNavigations';
import ScreenProfile from '../appScreens/ChatScreen/ScreenProfile';


const Stack = createNativeStackNavigator();

export default function LoginNavigations() {
  return(
    <Stack.Navigator
      screenOptions={{headerShown: false, animationEnabled: false}}>
        <Stack.Screen name="ScreenWelcome" component={ScreenWelcome} />
        <Stack.Screen name="ScreenLogin" component={ScreenLogin} />
        <Stack.Screen name="ScreenOtp" component={ScreenOtp} />
        <Stack.Screen name = "AppNavigations" component = {AppNavigations} />
        <Stack.Screen name="ScreenProfile" component={ScreenProfile} />

      </Stack.Navigator>
  )
  
}

const styles = StyleSheet.create({})