
import React, { useState } from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Header from '../components/Header';
import { colors } from '../utility/Color';
import { fonts } from '../utility/fonts';
import ScreenChat from '../appScreens/ChatScreen/ScreenChat';
import ScreenCall from '../appScreens/CallScreen/ScreenCall';
import ScreenStatus from '../appScreens/StatusScreen/ScreenStatus';
import { View, Text } from 'react-native';

const Tab = createMaterialTopTabNavigator();

export default function StackApp() {
  const [showView , setShowView] = useState(false);
  // const [inputView ,setInputView] = useState(false);

  console.log(typeof("setShowiew",setShowView))
  return (
    <>
      <Header searchState = {showView} onSearchPress={setShowView} />
      {/* states={showView} onPress = {setShowView}/> */}
      <Tab.Navigator
        screenOptions={{
          tabBarActiveTintColor: colors.WhiteColor,
          tabBarLabelStyle: {fontSize: 16, fontWeight: fonts.Bold},
          tabBarStyle: {backgroundColor: colors.BlueColor},
          tabBarIndicatorStyle: {
            borderBottomColor: colors.WhiteColor,
            borderBottomWidth: 3,
            width: '30%',
            left: '2%',
            right: '2%',
          },
        }}>
        <Tab.Screen name="CHATS" component={ScreenChat} />
        <Tab.Screen name="STATUS" component={ScreenStatus} />
        <Tab.Screen name="CALLS" component={ScreenCall} />
      </Tab.Navigator>
      {/* { 
      showView ?
      <View style={{position: 'absolute', top: 0 }}><Text>Hello DivYAnshu</Text></View>
      : null
      } */}
    </>
  );
}
