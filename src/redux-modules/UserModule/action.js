
import axios from 'axios';
import { FETCH_USER_DETAILS } from "./constants";
import { FETCH_USER_SUCCESS } from "./constants";
import { FETCH_USER_FAILURE } from "./constants";

const fetch_user_details = () => {
    return ({
        type : FETCH_USER_DETAILS,

    })
}

const fetch_user_success = (data) => {
    return({
        type : FETCH_USER_SUCCESS,
        payload : data
    })
}

const fetch_user_failure = (error) => {
    return({
        type : FETCH_USER_FAILURE,
        payload : error
    })
}

export const fetch_data = () => {
    return dispatch => {
        dispatch(fetch_user_details);
        axios.get("https://jsonplaceholder.typicode.com/users")
        .then(res => {
            const response = res.data
            dispatch(fetch_user_success(response))
        })
        .catch(err => {
            const error_message = err
            dispatch(fetch_user_failure(error_message))
        })
    }
}