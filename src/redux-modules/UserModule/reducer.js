
import { FETCH_USER_DETAILS } from "./constants";
import { FETCH_USER_SUCCESS } from "./constants";
import { FETCH_USER_FAILURE } from "./constants";

const initial_state = {
    user_data : [],
    error : "",
    loading : false,
}

const user_reducer = (state = initial_state,action) => {
    switch(action.type){
        case FETCH_USER_DETAILS:
            return {
                loading : true
            }
        case FETCH_USER_SUCCESS:
            return {
                loading : false,
                user_data : action.payload,
                error : ""
            }
        case FETCH_USER_FAILURE:
            return {
                loading : false,
                error : action.payload,
                user_data : []
            }
        default : return state
    }
}

export default user_reducer