import React,{useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AppNavigations from './src/navigations/AppNavigations';
import LoginNavigations from './src/navigations/LoginNavigations';
import ScreenSplash from './src/components/ScreenSplash';



export default function App() {
  const [isLoad, setIsLoad] = useState(true);
  setTimeout(() => {
    setIsLoad(false);
  }, 3000);
  return isLoad ? (
    <ScreenSplash />
  ) : (
    <NavigationContainer>
      <LoginNavigations />
    </NavigationContainer>
  );
}
